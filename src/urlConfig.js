const baseUrl = process.env.REACT_APP_API_KEY  || "https://flipkart-rest-server.herokuapp.com";
//const baseUrl = "http://localhost:2000";

export const api = `${baseUrl}/api`;

export const generatePublicUrl = (fileName) => {
  return `${baseUrl}/public/${fileName}`;
};
